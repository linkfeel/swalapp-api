<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Auth\AuthController@login')->name('login');
    Route::post('register', 'Auth\AuthController@register');
    Route::post('sendSms', 'Auth\AuthController@sendSms');
    Route::post('verifyPhone', 'Auth\AuthController@verifyPhone');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Auth\AuthController@logout');

    });
});

Route::group([
    'prefix' => 'user'
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('profile', 'User\UserController@GetUser');
        Route::get('club', 'User\UserController@GetClub');
        Route::get('centerinterest', 'User\UserController@GetCenterInterest');
        Route::post('userclub', 'User\UserController@AddUserClub');
        Route::post('userinterest', 'User\UserController@AddUserCenterInterest');
        Route::get('listclubuser', 'User\UserController@GetClubUser');
        Route::get('restlistclubuser', 'User\UserController@GetRestClubUser');
        Route::post('updateprofile', 'User\UserController@UpdateProfile');
    });
});

Route::group([
    'prefix' => 'club'
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('show', 'Club\ClubController@GetClub');

    });
});

Route::group([
    'prefix' => 'event'
], function () {
    Route::group([
        //'middleware' => 'auth:api'
    ], function() {
        Route::get('{club_id}', 'Event\EventController@get');
        Route::post('listevent', 'Event\EventController@GetListEvent');
        Route::post('addinterestevent', 'Event\EventController@UseInterestEvent');


    });
});

Route::group([
    'prefix' => 'booking'
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('addbooking', 'Booking\BookingController@AddBooking');

    });
});

Route::group([
        'prefix' => 'checkin'
    ], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('verifychekin', 'Checkin\CheckinController@VerifyCheckinUser');

    });
});


Route::group([
        'prefix' => 'checkin'
    ], function () {
    Route::get('qrcode', function () {
        return QrCode::size(300)->generate('Carp diem');
    });
});
