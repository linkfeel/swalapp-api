<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$dataJson = array('0' => 1,'1' => 1,'2' => 1,'3' => 1,'4' => 0,'5' => 0,'6' => 0);
    	$json = json_encode($dataJson);
        DB::table('events')->insert([
        	[
            'title' => 'Event 1',
            'image' => '',
            'description' => '',
            'started_date' => '2020-03-19 21:00:00',
            'ending_date' => '2020-03-20 21:00:00',
        	'club_id' => 1,
        	'repeat' => 0,
      		'days_available'=>NULL
        	],
        	[
            'title' => 'Event 2',
            'image' => '',
            'description' => '',
            'started_date' => '2020-03-20 21:00:00',
            'ending_date' => '2020-04-21 21:00:00',
        	'club_id' => 1,
        	'repeat' => 1,
      		'days_available'=>$json
        	],
        	[
            'title' => 'Event 3',
            'image' => '',
            'description' => '',
            'started_date' => '2020-04-04 21:00:00',
            'ending_date' => '2020-03-05 01:00:00',
        	'club_id' => 1,
        	'repeat' => 0,
      		'days_available'=>NULL
        	]
        ]);
    }
}
