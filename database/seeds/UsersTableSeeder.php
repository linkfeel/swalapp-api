<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'John',
            'last_name' => 'Smith',
            'phone' => '73777777',
            'email' => 'john@me.com',
            'password' => Hash::make('test'),
        ]);
    }
}
