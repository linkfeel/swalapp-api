<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersInterestedEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_interested_events', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('user_id')->unsigned()->nullable();
			$table->bigInteger('event_id')->unsigned()->nullable();
			$table->enum('action', array('not_going', 'going', 'interested'));
			$table->timestamps();
        });
        Schema::table('users_interested_events', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('event_id')->references('id')->on('events');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_interested_events');

        Schema::table('users_interested_events', function(Blueprint $table) {
			$table->dropForeign('users_interested_events_user_id_foreign');
			$table->dropForeign('users_interested_events_event_id_foreign');
		});
    }
}
