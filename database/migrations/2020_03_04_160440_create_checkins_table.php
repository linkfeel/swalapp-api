<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
			$table->json('location');
			$table->bigInteger('club_id')->unsigned()->nullable();
			$table->bigInteger('qrcode_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('checkins', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('club_id')->references('id')->on('clubs');
            $table->foreign('qrcode_id')->references('id')->on('qrcodes');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkins', function(Blueprint $table) {
            $table->dropForeign('checkins_user_id_foreign');
            $table->dropForeign('checkins_club_id_foreign');
            $table->dropForeign('checkins_qrcode_id_foreign');
        });
        Schema::dropIfExists('checkins');
    }
}
