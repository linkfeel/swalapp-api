<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAttributsToClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->string('company_name')->nullable();
            $table->text('logo')->nullable();
            $table->json('location')->nullable();
            $table->bigInteger('satus_id')->unsigned()->nullable();
            $table->integer('grade_id')->nullable();
            $table->softDeletes();
        });

        Schema::table('clubs', function(Blueprint $table) {
            $table->foreign('satus_id')->references('id')->on('status');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropForeign('clubs_satus_id_foreign');
        });
        Schema::drop('clubs');
    }
}
