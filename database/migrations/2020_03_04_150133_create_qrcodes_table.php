<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQrcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qrcodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('content');
            $table->bigInteger('club_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('qrcodes', function(Blueprint $table) {
			$table->foreign('club_id')->references('id')->on('clubs');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('qrcodes', function(Blueprint $table) {
			$table->dropForeign('qrcodes_club_id_foreign');
        });
        Schema::dropIfExists('qrcodes');
    }
}
