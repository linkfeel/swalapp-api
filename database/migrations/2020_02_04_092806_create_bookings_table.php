<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
			$table->bigInteger('club_id')->unsigned()->nullable();
			$table->smallInteger('number_of_participents');
			$table->datetime('booking_date');
			$table->enum('booking_status', array('pending', 'confirmed', 'refused', 'need_response'))->default('pending');
			$table->enum('booking_for', array('events', 'programs', 'clubs'));
			$table->bigInteger('booking_for_id');
			$table->text('comment')->nullable();
            $table->timestamps();
        });

        Schema::table('bookings', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
			$table->foreign('club_id')->references('id')->on('clubs')
						->onDelete('cascade')
						->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('bookings');
		Schema::table('bookings', function(Blueprint $table) {
			$table->dropForeign('bookings_customer_id_foreign');
			$table->dropForeign('bookings_club_id_foreign');
		});
    }
}
