<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('title');
			$table->text('image')->nullable();
            $table->text('description')->nullable();
            $table->json('location')->nullable();
			$table->text('themes_id')->nullable();
            $table->dateTime('started_date')->nullable();
            $table->dateTime('ending_date')->nullable();
			$table->dateTime('scheduled_publish_date')->nullable();
			$table->boolean('active')->default(1);
			$table->bigInteger('club_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();
        });

        Schema::table('events', function(Blueprint $table) {
			$table->foreign('club_id')->references('id')->on('clubs');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('events', function(Blueprint $table) {
			$table->dropForeign('events_club_id_foreign');
		});
		
		Schema::drop('events');
    }
}
