<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('user_club_id')->unsigned()->nullable();
            $table->bigInteger('user_center_interest_id')->unsigned()->nullable();
        });
        Schema::table('users', function($table) {
            $table->foreign('user_club_id')->references('id')->on('user_clubs');
            $table->foreign('user_center_interest_id')->references('id')->on('user_center_interests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_user_club_id_foreign');
            });
            Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_user_center_interest_id_foreign');
            });
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
