<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Models\Club;
use App\Models\UserClub;
use App\Models\UserCenterInterest;
use App\Models\CenterInterest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function GetUser(Request $request)
    {
        //test user
        if (!$request->user()){
            return response()->json([
                'message' => 'User not found'
            ], 404);
        }
        return response()->json($request->user());
    }
    /**
     * Get all list Club
     *
     * @return [json] club object
     */
    public function GetClub()
    {
        $club = Club::orderByRaw('name ASC')->get();
        //test list club
        if (!$club){
            return response()->json([
                'message' => 'List not found'
            ], 404);
        }
        return response()->json($club, 200);
    }
    /**
     * Get all list center interest
     *
     * @return [json] center interest object
     */
    public function GetCenterInterest()
    {
        $centerinterest = CenterInterest::orderByRaw('name ASC')->get();
        //test list center interer
        if (!$centerinterest){
            return response()->json([
                'message' => 'List not found'
            ], 404);
        }
        return response()->json($centerinterest, 200);
    }
     /**
     * Post add user club
     *
     * @return [json] message
     */
    public function AddUserClub(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'idclub' => 'required'
        ]);
        if ($validator->fails()) {

            return response()->json($validator->messages()->first(), 400);
       }
        //test if user have a userClub
        $iduser = $request->user()->id;
        $uc = UserClub::where('user_id', '=', $iduser)
                            ->first();
        if($uc)
        {
            $uc->club_id = $request->idclub;
            $updateuc= $uc->save();
            //test request
            if(!$updateuc){
                return response()->json([
                    'message' => 'Error update request'
                ], 500);
            }
            return response()->json([
                'message' => 'Successfully updated club of user!'
            ], 200);
        }
        else{
        //add userClub
            $UserClub = new UserClub;
            $UserClub->user_id = $iduser;
            $UserClub->club_id = $request->idclub;
            $addUserClub = $UserClub->save();
            //test request add
            if(!$addUserClub){
                return response()->json([
                    'message' => 'Error add request'
                ], 500);
            }
            $iduserclub = $UserClub->id;
            //update column idUserClub in table users
            $user = User::find($iduser);
            if(!$user) {
                return response()->json([
                    'message' => 'User not found'
                ], 404);
            }
                $user->user_club_id = $iduserclub;
                $updateuser = $user->save();
                //test request update
                if(!$updateuser){
                    return response()->json([
                        'message' => 'Error update request'
                    ], 500);
                }
                //success add
            return response()->json([
                'message' => 'Successfully saved user club!'
            ], 201);
        }
    }
     /**
     * Post add user center interest
     *
     * @return [json] message
     */
    public function AddUserCenterInterest(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'idcenterinterest' => 'required'
        ]);
        if ($validator->fails()) {

            return response()->json($validator->messages()->first(), 400);
       }
        //test if user have a centerinterer
        $iduser = $request->user()->id;
        $uc = UserCenterInterest::where('user_id', '=', $iduser)
                                    ->first();

        if($uc)
        {
            //update interest center
            $uc->center_interest_id = $request->idcenterinterest;
            $updateuc = $uc->save();
                //test request
                if(!$updateuc){
                    return response()->json([
                        'message' => 'Error update request'
                    ], 500);
                }
            return response()->json([
                'message' => 'Successfully updated interest center of user!'
            ], 200);
        }
        else{
        //add interest center of user
            $UserCenterInterest = new UserCenterInterest;
            $UserCenterInterest->user_id = $iduser;
            $UserCenterInterest->center_interest_id = $request->idcenterinterest;
            $addUserCenterInterest = $UserCenterInterest->save();
                //test request add
                if(!$addUserCenterInterest){
                    return response()->json([
                        'message' => 'Error add request'
                    ], 500);
                }
            $idUserCenterInterest = $UserCenterInterest->id;

            //update column idUserInterer in table users
            $user = User::find($iduser);
                //test user
                if(!$user) {
                    return response()->json([
                        'message' => 'User not found'
                    ], 404);
                }
            $user->user_center_interest_id = $idUserCenterInterest;
            $updateuser = $user->save();
                //test request update
                if(!$updateuser){
                    return response()->json([
                        'message' => 'Error update request'
                    ], 500);
                }
            //success add
            return response()->json([
                'message' => 'Successfully saved interst center!'
            ], 201);
        }
    }
    /**
     * Get list club user
     *
     * @return [json] club object
     */
    public function GetClubUser(Request $request)
    {
        $listClubs= array();
        //get id user club
        $idUserClub = $request->user()->user_club_id;
        //get list club of user
        $clubuser = UserClub::find($idUserClub);
        $listclubuser =json_decode($clubuser->club_id)->idclubs;
        //get details of club
        foreach ($listclubuser as $listclubusers) {
            $club = Club::find( $listclubusers);
            array_push($listClubs, $club);
        }
        //test array empty
        if (sizeof($listClubs) > 0){
            return response()->json($listClubs, 200);
        }
        else{
            return response()->json([
                'message' => 'List not found'
            ], 404);
        }

    }
    /**
     * Get rest list club for user
     *
     * @return [json] club object
     */
    public function GetRestClubUser(Request $request)
    {
        //get id user club
        $idUserClub = $request->user()->user_club_id;
        //get list club of user
        $clubuser = UserClub::find($idUserClub);
        $listclubuser =json_decode($clubuser->club_id)->idclubs;
        $restlistClubs = Club::whereNotIn('id', $listclubuser)
                    ->orderByRaw('name ASC')
                    ->get();

        //test array empty
        if (sizeof($restlistClubs) > 0){
            return response()->json($restlistClubs, 200);
        }
        else{
            return response()->json([
                'message' => 'List not found'
            ], 404);
        }
    }
    /**
     * Post update profile
     *
     * @return [json] message
     */
    public function UpdateProfile(Request $request)
    {
        //get id user
        $idUser = $request->user()->id;
        //get list club of user
        $user = User::find($idUser);
            //test user
            if(!$user){
                return response()->json([
                    'message' => 'User not found'
                ], 404);
            }
        $user->first_name = $request->fName;
        $user->last_name = $request->lName;
        $user->tel = $request->tel;
        $updateuser = $user->save();
            //test request update
            if(!$updateuser){
                return response()->json([
                    'message' => 'Error update request'
                ], 500);
            }
        return response()->json([
            'message' => 'Successfully updated'
        ], 200);
    }
}
