<?php

namespace App\Http\Controllers\Event;

use App\Models\Club;
use App\Models\Event;
use App\Models\Bookings;
use App\Models\UsersInterestedEvents;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
class EventController extends Controller
{

    /**
     * @OA\GET(
     *     path="/api/event/{club_id}",
     *     tags={"Event List"},
     *     summary="Returns Program of club",
     *     description="A sample greeting to test out the API",
     *     operationId="sampleFunctionWithDoc",
            @OA\Parameter(
                name="club_id",
                in="path",
                required=true
            ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     @OA\Tag(
 *     name="Event List",
 *     description="Sample package to test out the event APIs",
 * )
     */

    public function get(Request $request, $club_id)
    {
        $startOfWeek = Carbon::now()->startOfWeek()->startOfDay();
        $endOfWeek = Carbon::now()->endOfWeek()->endOfDay();
        $user_id = $request->user()->id;

        $list = Event::where('club_id', '=', $club_id)
                        ->where('active',1)
                        ->where(function ($query) use ($startOfWeek, $endOfWeek) {
                            return $query->whereBetween('started_date', [$startOfWeek, $endOfWeek])
                                            ->orWhere('ending_date', '>', $endOfWeek);
                        })
                        ->withCount(['interest', 'going', 'booking'])
                        ->with(['user'=> function ($q) use ($user_id){
                            $q->where('user_id', $user_id);   
                        },'userbooking'=> function ($q) use ($user_id){
                            $q->where('user_id', $user_id);   
                        }])
                        ->get();

        $weekEvent = array();
        foreach ($list as $key => $item) {
            if($item->repeat){
                if($item->days_available){
                    foreach ($item->days_available as $day => $value) {
                        if($value) 
                            $weekEvent["-$day"][]=$item;
                    }
                }
            }else{
                $dayofweek = date('w', strtotime($item->started_date));
                $weekEvent["-$dayofweek"][]=$item;
            }
        }
        //test list not empty
        if (sizeof($weekEvent) > 0){
            return response()->json($weekEvent, 200);
        }
        else{
            return response()->json([
                'message' => 'List not found'
            ], 404);
        }

    }
    /**
     * Get list event Club
     *
     * @return [json] event object
     */
    public function GetListEvent(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'idclub' => 'required|integer'
        ]);
        if ($validator->fails()) {

            return response()->json($validator->messages()->first(), 400);
       }
       //get list event by club
        $event = Event::where('club_id', '=', $request->idclub)
                        ->orderByRaw('created_at DESC')
                        ->get();
        $alllist = array();
        $nbpersonsinterested =0;
        $nbpersonsgoing =0;
        $useractions = '';
        $user_booked = true;
        foreach ($event as $e) {
            //get number person interested in event
            $nbpersoninterest = UsersInterestedEvents::where('event_id', '=', $e->id)
                                                    ->where('action','LIKE','%interested%')
                                                    ->count();
            if ($nbpersoninterest){
            $nbpersonsinterested = $nbpersoninterest;
            }
            else{
                $nbpersonsinterested = 0;
            }
            //get number person going to event
            $nbpersongoing = UsersInterestedEvents::where('event_id', '=', $e->id)
            ->where('action','LIKE','%going%')
            ->count();
            if ($nbpersongoing){
            $nbpersonsgoing = $nbpersongoing;
            }
            else{
            $nbpersonsgoing = 0;
            }
            //get action user in event
            $useraction = UsersInterestedEvents::where('event_id', '=', $e->id)
                                                ->where('user_id','=',$request->user()->id)
                                                ->get();
            if($useraction){
                foreach ($useraction as $ua) {
                    $useractions = $ua->action;
                }
            }
            else{
                $useractions = null;
            }
            //get current number person booked
            $booking = Bookings::where('booking_for', '=', 'events')
                                                ->where('booking_for_id','=',$e->id)
                                                ->where('booking_status','=','confirmed')
                                                ->get();
            $total_person_booked =0;
                if($booking){
                    foreach ($booking as $b) {
                    $total_person_booked += $b->number_of_participents;
                    }
                }
            //get user booked
            $userbooked = Bookings::where('booking_for', '=', 'events')
                                ->where('booking_for_id','=',$e->id)
                                ->where('user_id','=',$request->user()->id)
                                ->first();
            if(!$userbooked){
                $user_booked = false;
            }

            array_push($alllist,
            array(
            'event' => $e,
            'nb_person_interested' => $nbpersonsinterested,
            'nb_person_going' => $nbpersonsgoing,
            'user_action' => $useractions,
            'total_person_booking' => $total_person_booked,
            'user_booked' => $user_booked
            )
            );
        }
        //test list not empty
        if (sizeof($alllist) > 0){
            return response()->json($alllist, 200);
        }
        else{
            return response()->json([
                'message' => 'List not found'
            ], 404);
        }
    }
    /**
     * Post interest event user
     *
     * @return [json] event object
     */
    public function UseInterestEvent(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'idevent' => 'required|integer',
            'action' => 'required|string'
            ]);
        if ($validator->fails()) {

            return response()->json($validator->messages()->first(), 400);
       }
        //test if user interested from club
        $iduser = $request->user()->id;
        $uie = UsersInterestedEvents::where('user_id', '=', $iduser)
                                    ->where('event_id', '=', $request->idevent )
                            ->first();
        if( $uie)
        {
            //update action
            $uie->action = $request->action;
           $updateinterestuser = $uie->save();
            //test request
            if(!$updateinterestuser){
                return response()->json([
                    'message' => 'Error update request'
                ], 500);
            }
            return response()->json([
                'message' => 'Successfully updated interest user event!'
            ], 200);
        }
        else{
        //add user interest event
            $UsersInterestedEvents = new UsersInterestedEvents;
            $UsersInterestedEvents->user_id = $iduser;
            $UsersInterestedEvents->event_id = $request->idevent;
            $UsersInterestedEvents->action = $request->action;
            $UsersInterestedEvents->save();
            //test request
            if(!$UsersInterestedEvents){
                return response()->json([
                    'message' => 'Error saved request'
                ], 500);
            }
            return response()->json([
                'message' => 'Successfully added interest user event!'
            ], 201);

        }

    }
}
