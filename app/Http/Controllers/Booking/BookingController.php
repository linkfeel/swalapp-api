<?php

namespace App\Http\Controllers\Booking;

use App\Models\Club;
use App\Models\Event;
use App\Models\Bookings;
use App\Models\UsersInterestedEvents;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    /**
     * Post add booking user
     *
     * @return [json] message success saved
     */
    public function AddBooking(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'idClub' => 'required|integer',
            'nbParticipents' => 'required|integer',
            'bookingDate' => 'required',
            'bookingFor' => 'required|string',
            'bookingForId' => 'required|integer'
        ]);
        if ($validator->fails()) {

            return response()->json($validator->messages()->first(), 400);
       }
        //test if user have booking
        $iduser = $request->user()->id;

        $userbooking = Bookings::where('user_id', '=', $iduser)
                        ->where('booking_for_id', '=', $request->bookingForId)
                        ->first();
        if($userbooking)
        {
            //update booking
            $userbooking->booking_date = $request->bookingDate;
            $userbooking->comment = $request->comment;
            $userbooking->number_of_participents = $request->nbParticipents;
            $updateuserbooking = $userbooking->save();
                //test request
                if(!$updateuserbooking){
                    return response()->json([
                        'message' => 'Error update request'
                    ], 500);
                }
            return response()->json([
                'message' => 'Successfully updated booking of user!'
            ], 200);
        }
        else{
        //add booking of user
            $UserBooking = new Bookings;
            $UserBooking->user_id = $iduser;
            $UserBooking->club_id = $request->idClub;
            $UserBooking->number_of_participents = $request->nbParticipents;
            $UserBooking->booking_date = $request->bookingDate;
            $UserBooking->booking_for = $request->bookingFor;
            $UserBooking->booking_for_id = $request->bookingForId;
            $UserBooking->comment = $request->comment;
            $addUserBooking = $UserBooking->save();
                //test request add
                if(!$addUserBooking){
                    return response()->json([
                        'message' => 'Error add request'
                    ], 500);
                }
            //success add
            return response()->json([
                'message' => 'Successfully saved booking!'
            ], 201);
        }
    }

}
