<?php

namespace App\Http\Controllers\Club;

use App\Models\Club;
use App\Models\Event;
use App\Models\Bookings;
use App\Models\UsersInterestedEvents;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ClubController extends Controller
{
    /**
     * Get one Club
     *
     * @return [json] club object
     */
    public function GetClub(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'idclub' => 'required|integer'
        ]);
        if ($validator->fails()) {

            return response()->json($validator->messages()->first(), 400);
       }
        $club = Club::find($request->idclub);
        //test list club
        if (!$club){
            return response()->json([
                'message' => 'List not found'
            ], 404);
        }
        return response()->json($club, 200);
    }

}
