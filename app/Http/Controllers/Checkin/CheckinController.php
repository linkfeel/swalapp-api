<?php

namespace App\Http\Controllers\Checkin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Qrcode;
use App\Models\Checkin;

class CheckinController extends Controller
{

    /**
     * Post check In
     *
     * @return [json] message
     */
    public function VerifyCheckinUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'content' => 'required|string',
            'location' => 'required|string'
        ]);
        if ($validator->fails()) {

            return response()->json($validator->messages()->first(), 400);
       }

        $qrcode = Qrcode::where('content', '=', $request->content)
                            ->first();

        //verify qrcode existed
        if (!$qrcode){
            return response()->json([
                'message' => 'QrCode not found'
            ], 404);
        }
        //save checkin user
        $checkin = new Checkin;
        $checkin->user_id = $request->user()->id;
        $checkin->location = $request->location;
        $checkin->club_id = $qrcode->club_id;
        $checkin->qrcode_id = $qrcode->id;
        $checkin->save();
            //test request checkin
            if(!$checkin){
                return response()->json([
                    'message' => 'Error saved request'
                ], 500);
            }
        return response()->json([
            'message' => 'Checkin saved']
        , 200);
    }
}
