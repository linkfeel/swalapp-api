<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Club;
use App\Models\UserClub;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Validator;
use Twilio\Exceptions\TwilioException;

class AuthController extends Controller
{
    public function login(Request $request) {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            //'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    /**
     * Start a phone verification process using Twilio Verify V2 API
     *
     * @param $request
     * @return Json
     */
    public function sendSms( Request $request )
    {
        $validator = Validator::make($request->all(), [
            'tel' => 'required'
        ]);
        if ($validator->fails()) {

            return response()->json($validator->messages()->first(), 400);
       }
        /* Get credentials from .env */
        $token = "10a1fff832f7d4126b81d293c35f9de9";
        $twilio_sid = "AC748927cf7ba4d185a5fa636f080446e0";
        $twilio_verify_sid = "VAcae567efcf104994b28f366eed50fdd4";
        $twilio = new Client($twilio_sid, $token);

        //for create a new phone
        $verifications = $twilio->verify->v2->services($twilio_verify_sid)
            ->verifications
            ->create($request->tel, "sms");

            try {

                if ($verifications->status ==  "pending"){
                    return response()->json([
                        'message' => 'Successfully recieved verification code'
                    ], 201);
                }

            }
            catch (TwilioException $exception) {
                return response()->json(['message' =>
                "Verification failed to start: {$exception->getMessage()}"]);
            }

    }

    /**
     * Verify Phone code using Twilio Verify V2 API
     *
     * @param $request
     * @return Json
     */
    public function verifyPhone( Request $request )
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'fName' => 'required|string',
            'lName' => 'required|string',
            'tel' => 'required',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ]);
        if ($validator->fails()) {

            return response()->json($validator->messages()->first(), 400);
       }

        /* Get credentials from .env */
        $token = "10a1fff832f7d4126b81d293c35f9de9";
        $twilio_sid = "AC748927cf7ba4d185a5fa636f080446e0";
        $twilio_verify_sid = "VAcae567efcf104994b28f366eed50fdd4";
        $twilio = new Client($twilio_sid, $token);

        try {
        //Check verification code
        $verification_check = $twilio->verify->v2->services($twilio_verify_sid)
            ->verificationChecks
            ->create($request->code, // code
            array("to" => $request->tel)
            );

            //test verification success
            if ($verification_check->status ===  'approved'){

                //register user
               if( $this->register($request->fName,
                $request->lName,
                $request->tel,
                $request->email,
                $request->password) == false )

                {
                    return response()->json([
                        'message' => 'Error saved user'
                    ], 404);

                }

                return response()->json([
                    'message' => 'Successfully created user!'
                ], 201);

            }
            return response()->json(['message' =>
            'Verification check failed: Invalid code.']);
        }
        catch (TwilioException $exception) {
            return response()->json(['message' =>
            "Verification check failed: {$exception->getMessage()}"]);
        }
    }



    public function register($fName,$lName,$tel,$email,$password)
    {
        $user = new User;
        $user->first_name = $fName;
        $user->last_name = $lName;
        $user->tel = $tel;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();

        if (!$user){
            return false;
        }
        return true;

    }


    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

}
