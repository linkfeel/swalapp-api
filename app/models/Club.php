<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $casts = [
        'location' => 'array'
    ];
    protected $fillable = [
        'name','comapny_name','logo','location','status_id',
        'grade_id'
    ];

}
