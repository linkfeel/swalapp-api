<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class Qrcode extends Model
{
    protected $fillable = [
        'content','club_id'
    ];
}
