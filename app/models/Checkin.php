<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checkin extends Model
{
    protected $casts = [
        'location' => 'array'
    ];
    protected $fillable = [
        'user_id','club_id','qrcode_id'
    ];
}
