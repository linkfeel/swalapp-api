<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    protected $fillable = [
        'user_id','club_id','number_of_participents',
        'booking_date','booking_status','booking_for',
        'booking_for_id','comment'
    ];

    public function user()
    {
        $this->belongsTo('App\Models\User');
    }
}
