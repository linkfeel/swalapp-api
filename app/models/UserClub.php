<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserClub extends Model
{

    
    protected $casts = [
        'club_id' => 'array'
    ];
    protected $fillable = [
        'user_id','club_id'
    ];
}
