<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersInterestedEvents extends Model
{
    protected $fillable = [
        'user_id','event_id','action'
    ];
}
