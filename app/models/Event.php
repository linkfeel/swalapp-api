<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $casts = [
        'location' => 'array',
        'days_available' => 'array'
    ];
    protected $fillable = [
        'title','image','description','themes_id','started_date',
        'ending_date','location','active','club_id', 'hour', 'repeat', 'days_available'
    ];

    public function interest()
	{
   		return $this->hasMany('App\Models\UsersInterestedEvents')->where('action', 'interested');
	}

	public function going()
	{
   		return $this->hasMany('App\Models\UsersInterestedEvents')->where('action', 'going');
	}

	public function user()
	{
   		return $this->hasOne('App\Models\UsersInterestedEvents');
	}

	public function booking()
	{
   		return $this->hasMany('App\Models\Bookings', 'booking_for_id')->where('booking_for', 'events')->where('booking_status', 'confirmed');
	}

	public function userbooking()
	{
   		return $this->hasOne('App\Models\Bookings', 'booking_for_id')->where('booking_for', 'events');
	}
}
