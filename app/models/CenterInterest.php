<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class CenterInterest extends Model
{
    protected $fillable = [
        'name'
    ];
}
