<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class UserCenterInterest extends Model
{
    protected $appends = [
        'center_interest_id' => 'array'
    ];
    protected $fillable = [
        'user_id'
    ];
    
}
